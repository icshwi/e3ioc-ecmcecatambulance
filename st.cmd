epicsEnvSet IOC "ECAT-AMB:"

require ecmccfg 8.0.0
iocshLoad "${ecmccfg_DIR}/startup.cmd" "ECMC_VER=8.0.2, NAMING=ESSnaming"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EK1100
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL1808
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL2819
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL5101
iocshLoad "${ecmccfg_DIR}/configureSlave.cmd" "HW_DESC=EL7041-0052, CONFIG=-Motor-Nanotec-ST4118M1804-B"

ecmcConfigOrDie "Cfg.EcAddEntryDT(${ECMC_EC_SLAVE_NUM},0x2,0x1b873052,2,3,0x1a04,0x6010,0x11,S16,infoData01,1)"
ecmcConfigOrDie "Cfg.EcAddEntryDT(${ECMC_EC_SLAVE_NUM},0x2,0x1b873052,2,3,0x1a04,0x6010,0x12,S16,infoData02,1)"

ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,1000,2)"
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x5,5,1)"
#ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x11,?,1)"
#ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x19,?,1)"

iocshLoad "${ecmccfg_DIR}/configureSlave.cmd" "HW_DESC=EL7047, CONFIG=-Motor-Nanotec-ST4118M1804-B"

ecmcConfigOrDie "Cfg.EcAddEntryDT(${ECMC_EC_SLAVE_NUM},0x2,0x1b873052,2,3,0x1a04,0x6010,0x11,S16,infoData01,1)"
ecmcConfigOrDie "Cfg.EcAddEntryDT(${ECMC_EC_SLAVE_NUM},0x2,0x1b873052,2,3,0x1a04,0x6010,0x12,S16,infoData02,1)"
ecmcConfigOrDie "Cfg.EcAddEntryDT(${ECMC_EC_SLAVE_NUM},0x2,0x1b873052,2,3,0x1a05,0x6010,0x13,S16,motorLoad,1)"

# Amplitude of phase current
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x1,1000,2)"
# Amplitude of phase reduced current
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0x2,0,2)"
# Set speed range to 32000 full steps per second
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x5,5,1)"
# Info data 1
#ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x11,?,1)"
# Info data 2
#ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x19,?,1)"
# Use EL7047 brake control instead of ecmc
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8012,0x3A,1,1)"

iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis1.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis2.ax"

iocshLoad "${ecmccfg_DIR}/applyConfig.cmd"
iocshLoad "${ecmccfg_DIR}/setAppMode.cmd"

dbLoadRecords "${E3_CMD_TOP}/db/infoData.db" "P=${SM_PREFIX}, MASTER_ID=${ECMC_EC_MASTER_ID}, PORT=${ECMC_ASYN_PORT}, ADDR=0, TIMEOUT=1, T_SMP_MS=1
